package hu.dudee.swag.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "examples")
public class ExampleEntity implements Serializable {

	private static final long serialVersionUID = -1710125812163415034L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String valami;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValami() {
		return valami;
	}

	public void setValami(String valami) {
		this.valami = valami;
	}

	@Override
	public String toString() {
		return "ExampleEntity [id=" + id + ", valami=" + valami + "]";
	}

}
