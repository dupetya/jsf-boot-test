package hu.dudee.swag.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.dudee.swag.entity.ExampleEntity;

public interface ExampleRepository extends JpaRepository<ExampleEntity, Long> {

}
