package hu.dudee.swag.web.ctrl;

import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import hu.dudee.swag.entity.ExampleEntity;
import hu.dudee.swag.repo.ExampleRepository;

@Component
@Scope("session")
public class MenuCtrl implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private transient MenuProvider menuProvider;
	
	@Autowired
	private ExampleRepository exampleRepository;
	
	@PostConstruct
	public void init() {
		ExampleEntity entity = exampleRepository.findOne(1L);
		if(entity == null) {
			entity = new ExampleEntity();
			entity.setValami("Elso valami");
			entity = exampleRepository.save(entity);
		}
		System.out.println(entity);
	}

	public List<MenuItem> getMenuItems() {
		return menuProvider.getMenu().stream().map(this::getAbsoluteMenuItem).collect(toList());
	}

	private MenuItem getAbsoluteMenuItem(MenuItem mi) {
		if (mi == null) {
			return null;
		}
		MenuItem res = new MenuItem();
		res.setLabel(mi.getLabel());
		res.setAction("/" + mi.getAction());

		return res;
	}

}
