package hu.dudee.swag.web.ctrl;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service("menuProvider")
public class DefaultMenuProviderImpl implements MenuProvider {

	@Override
	public List<MenuItem> getMenu() {
		return Arrays.asList(createMenuItem("index", "SwAg"), createMenuItem("pages/asd1", "AsD1"));
	}

	private MenuItem createMenuItem(String action, String label) {
		MenuItem itm1 = new MenuItem();
		itm1.setAction(action);
		itm1.setLabel(label);
		return itm1;
	}

}
