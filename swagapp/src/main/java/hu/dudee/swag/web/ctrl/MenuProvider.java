package hu.dudee.swag.web.ctrl;

import java.util.List;

public interface MenuProvider {
	List<MenuItem> getMenu();
}
