package hu.dudee.swag;

import javax.faces.webapp.FacesServlet;
import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ServletContextAware;

import hu.dudee.swag.web.scope.SessionReplicationAwareScopeMetadataResolver;

@Configuration
@ComponentScan(basePackages = { "hu.dudee" }, scopeResolver = SessionReplicationAwareScopeMetadataResolver.class)
@EnableAutoConfiguration
public class Main extends SpringBootServletInitializer implements ServletContextAware {
	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Main.class, Initializer.class, WebConfig.class);
	}

	@Bean
	public ServletRegistrationBean servletRegistrationBean() {
		FacesServlet servlet = new FacesServlet();
		ServletRegistrationBean regBean = new ServletRegistrationBean(servlet, "*.xhtml");
		regBean.setLoadOnStartup(1);
		return regBean;
	}

	@Override
	public void setServletContext(ServletContext arg0) {
		arg0.setInitParameter("com.sun.faces.forceLoadConfiguration", Boolean.TRUE.toString());
	}

	@Bean
	public DataSource dataSource() {
		return DataSourceBuilder.create().driverClassName("org.hsqldb.jdbcDriver").url("jdbc:hsqldb:file:data/db;")
				.username("sa").password("").build();
	}

}
